let gods = require('./arqex4.js')

// Mostrar nome e quantidade de features do deus:

for(i=0; i<gods.length; i++){
            console.log((gods[i].name), (gods[i].features.length))
}

// Selecionando campeões da role Mid:

for(i=0; i<gods.length; i++){
    if(gods[i].roles == "Mid"){
        console.log(gods[i])
    }
}

// Ordenando de acordo com o panteão:

console.log(gods.sort((a,b)=>a.pantheon<b.pantheon ? -1 : 0))

// Listando nomes seguidos de classe:

for(i=0; i<gods.length -1; i++){
    console.log(gods[i].name + ' ('+gods[i].class + ')')
}

// definir matrizes
var matrizresultante = []
var matriz1 = [[2, -1], [2, 0]]
var matriz2 = [[2, 3], [-2, 1]]
// verificar a possibilidade da multiplicação
if (matriz1.length === matriz2[0].length) {
    for (let a = 0; a < matriz2[0].length; a++) {
        matrizresultante.push([])

        for (let b = 0; b < matriz1.length; b++) {
            matrizresultante[a].push(0)
        }
    }

    for (i = 0; i < matriz1.length; i++) {
        for (j = 0; j < matriz2[0].length; j++) {
            for (let k = 0; k < matriz2.length; k++) {
                matrizresultante[i][j] += matriz2[k][j] * matriz1[i][k]
            }
        }
    }
    
    console.log(matrizresultante)
}
else {
    console.log('não é possível multiplicar tais matrizes')
}

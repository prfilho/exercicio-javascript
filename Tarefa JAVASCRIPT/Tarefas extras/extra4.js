nota = 90

if (nota >= 90) {
    console.log('Você tirou um A')
} else if (nota >= 80 && nota < 90) {
    console.log('Você tirou um B')
} else if (nota >= 70 && nota < 80) {
    console.log('Você tirou um C')
} else if (nota >= 60 && nota < 70) {
    console.log('Você tirou um D')
} else {
    console.log('Você tirou um F')
}
